//
//  Utils.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import Foundation
import SystemConfiguration
import UIKit

class Utils{

    static func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    
    }
    
    static func showMessage(title:String, message:String,viewController : UIViewController, buttonTitle : String){
       
        var action = [UIAlertAction]()
        
        action.append(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        
        showMessage(title: title, message: message, viewController: viewController, buttonTitle: "\(action)")
    }
    
    static func dateFormatter(date : String) -> String {
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        var getDate = Date()
        if let haveDate = dateFormatter.date(from: date){
        
            getDate = haveDate
        }
        
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.locale = tempLocale
        let dateString = dateFormatter.string(from: getDate)
        
        let dateAll = dateString
        
        return dateAll
    }
    
    static func getImage(urlString : String, imageView : UIImageView){
        
        if let url : URL = URL(string: urlString){
            
            let session = URLSession.shared
            let task = session.dataTask(with: url, completionHandler:{
                (data, response, error)in
                
                if data != nil{
                    if let image = UIImage(data: data!){
                        
                        DispatchQueue.main.async(execute: {
                            imageView.image = image
                        })
                    }
                }
            })
            
            task.resume()
        }
    }
    
}
