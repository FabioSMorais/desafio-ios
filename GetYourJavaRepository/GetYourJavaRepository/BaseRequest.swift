//
//  BaseRequest.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import Foundation
import Alamofire

class BaseRequest{

    struct TypeRequest {
        static let GET = "GET"
    }

    func getRepositoriesUrl(pageNumber : String) -> String{
        
        let BASE_SERVICES_REPOSITORIES = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(pageNumber)"

        return BASE_SERVICES_REPOSITORIES
    }
    
    func getPullRequestsUrl(creator: String, repository: String) -> String{
        
        let BASE_SERVICES_PULLREQUEST = "https://api.github.com/repos/\(creator)/\(repository)/pulls"
        
        return BASE_SERVICES_PULLREQUEST
    }
}
