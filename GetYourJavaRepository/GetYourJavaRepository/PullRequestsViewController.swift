//
//  PullRequestsViewController.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import UIKit
import SafariServices

class PullRequestsViewController: UIViewController {

    @IBOutlet weak var uiTablePRequests: UITableView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    
    var repositoryName = ""
    var userName = ""
    var arrayPullRequests = [PullRequestResponse]()
    var urlGitHub = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareTableView()
        self.uiTablePRequests.rowHeight = UITableViewAutomaticDimension
        self.uiTablePRequests.estimatedRowHeight =  130
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getData()
    }
    
    func getData(){
    
        self.myActivityIndicator.startAnimating()

        ServicesController().getPullRequest(creator: userName, repository: repositoryName) { (pullRequestResponse, webResponse) in
            
            if webResponse.isError{
                Utils.showMessage(title: "Atenção", message: "Ocorreu um erro...tente novamente mais tarde!", viewController: self, buttonTitle: "Ok")

                return
            }
        
            self.myActivityIndicator.stopAnimating()
            self.myActivityIndicator.hidesWhenStopped = true

            self.arrayPullRequests = pullRequestResponse

            self.uiTablePRequests.reloadData()

        }
    }
   
    func goToSafari(_ which: Int, url: String) {
        
        if let url = URL(string:  (urlGitHub)) {
            let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
            present(vc, animated: true)
        }
    }
}

extension PullRequestsViewController : UITableViewDelegate, UITableViewDataSource{

    func prepareTableView(){
        
        self.uiTablePRequests.delegate = self
        self.uiTablePRequests.dataSource = self
        self.uiTablePRequests.register(UINib(nibName: "PullRequestsTableViewCell", bundle: nil), forCellReuseIdentifier: "pullRequestCell")

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayPullRequests.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell  = self.uiTablePRequests.dequeueReusableCell(withIdentifier: "pullRequestCell") as!PullRequestsTableViewCell
       
        if arrayPullRequests.isEmpty == false{
            cell.textForCell(pullRequest: arrayPullRequests[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        urlGitHub = arrayPullRequests[indexPath.row].urlGiHub

        goToSafari(indexPath.row, url: urlGitHub )

    }
}

