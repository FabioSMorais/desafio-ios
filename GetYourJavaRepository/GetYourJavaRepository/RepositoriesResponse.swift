//
//  RepositoriesResponse.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 21/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import Foundation
import SwiftyJSON

class RepositoriesResponse{

    var repositoryName : String = ""
    var userName : String = ""
    var fullName : String = ""
    var authorImage : String = ""
    var description : String = ""
    var starsNumbers : String = ""
    var forksNumbers : String = ""

    class func returnRepositories(json : JSON) -> [RepositoriesResponse]{
    
        var arrayRepositories = [RepositoriesResponse]()
        if let jsonArray = json["items"].array{
            for item in jsonArray{
                let repositories = RepositoriesResponse()
                
                repositories.repositoryName = item["name"].stringValue
                repositories.userName = item["owner"]["login"].stringValue
                repositories.fullName = item["full_name"].stringValue
                repositories.authorImage = item["owner"]["avatar_url"].stringValue
                repositories.description = item["description"].stringValue
                repositories.starsNumbers = item["watchers"].stringValue
                repositories.forksNumbers = item["forks"].stringValue
                
                arrayRepositories.append(repositories)
            }            
            return arrayRepositories
        }
        return[]
    }
}
