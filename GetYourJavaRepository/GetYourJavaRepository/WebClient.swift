//
//  WebClient.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WebClient{

    func RequestRepositoriesWithURL(typeRequest : String, url : String, parameters : Dictionary<String, AnyObject>, callback : @escaping (WebResponse) -> Void){
    
        if !Utils.isConnectedToNetwork(){
            callback(WebResponse(isError: true, messageError: "Você não está conectado à Internet", json: JSON.null))
            return
        }
    
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON { (response) in
            
            print("Request: \(String(describing: response.request))")
            var jsonConverted : JSON?
            let _ : String = "Ocorreu um erro, tente novamente!"
            
            if response.result.value != nil{
                jsonConverted = JSON(response.result.value!)
                print("JSON Response: \(String(describing: jsonConverted))")
                
                callback(WebResponse(isError: false, messageError: "", json: jsonConverted!))
                
                return
            }
            // Error
            callback(WebResponse(isError: true, messageError: "Erro desconhecido", json: JSON.null))
        }
    }
}

