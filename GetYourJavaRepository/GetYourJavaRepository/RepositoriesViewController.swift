//
//  ViewController.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import UIKit


class RepositoriesViewController: UIViewController {

    @IBOutlet weak var uiTableRepositories: UITableView!
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
   
    var arrayRepositories = [RepositoriesResponse]()
    var pageNumber = 1
    var refreshControl = UIRefreshControl()
    var getRepositories = RepositoriesResponse()

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareTableView()
        pullToRefresh()
        self.uiTableRepositories.rowHeight = UITableViewAutomaticDimension
        self.uiTableRepositories.estimatedRowHeight =  130
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getData()
    }

    func getData(){
        
        self.myActivityIndicator.startAnimating()

        ServicesController().getRepositories(pageNumber: "\(pageNumber)") { (repositoriesResponse, webResponse) in
            
            if webResponse.isError{
                Utils.showMessage(title: "Atenção", message: "Ocorreu um erro...tente novamente mais tarde!", viewController: self, buttonTitle: "Ok")
                self.pageNumber -= 1
                return
            }
                        
            if repositoriesResponse.count != 0 {
            
                self.arrayRepositories += repositoriesResponse
               
            }else{
            
                self.pageNumber = -1
            }
            
            if repositoriesResponse.count < 30{
                self.pageNumber = -1
            }
            self.myActivityIndicator.stopAnimating()
            self.myActivityIndicator.hidesWhenStopped = true

            self.uiTableRepositories.reloadData()
        }
        self.refreshControl.endRefreshing()

        self.pageNumber += 1
        
    }
    
    func pullToRefresh(){
       
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControlEvents.valueChanged)
        uiTableRepositories.addSubview(refreshControl)
        
    }
    
    func refresh(sender:AnyObject) {

        self.pageNumber = 1
        arrayRepositories.removeAll()
        self.getData()
        refreshControl.endRefreshing()
    }

}

extension RepositoriesViewController : UITableViewDataSource, UITableViewDelegate{
    
   
    func prepareTableView(){
    
        self.uiTableRepositories.delegate = self
        self.uiTableRepositories.dataSource = self
        self.uiTableRepositories.register(UINib(nibName: "RepositoriesTableViewCell", bundle: nil), forCellReuseIdentifier: "repositoriesCell")
    
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return arrayRepositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.uiTableRepositories.dequeueReusableCell(withIdentifier: "repositoriesCell") as! RepositoriesTableViewCell
        
        if arrayRepositories.isEmpty == false{
        
         cell.textForCell(repository: arrayRepositories[indexPath.row] )

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PullRequestsViewController") as! PullRequestsViewController
    
        vc.repositoryName = arrayRepositories[indexPath.row].repositoryName
        vc.userName = arrayRepositories[indexPath.row].userName
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height){
            
            getData()
        }
    }
}
