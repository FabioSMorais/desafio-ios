//
//  serviceController.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 21/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import Foundation

class ServicesController : BaseRequest{

    func getRepositories(pageNumber : String, callback : @escaping ([RepositoriesResponse], WebResponse) -> Void){
        WebClient().RequestRepositoriesWithURL(typeRequest: TypeRequest.GET, url: getRepositoriesUrl(pageNumber: pageNumber), parameters: [:]) { (webResponse) in
            callback(RepositoriesResponse.returnRepositories(json: webResponse.json), webResponse)
    
        }
    }
    
    func getPullRequest(creator : String, repository: String, callback: @escaping ([PullRequestResponse], WebResponse) -> Void ){
        WebClient().RequestRepositoriesWithURL(typeRequest: TypeRequest.GET, url: getPullRequestsUrl(creator: creator, repository: repository), parameters: [:]) { (webResponse) in
            callback(PullRequestResponse.returnPullRequests(json: webResponse.json), webResponse)
        }
    }
}
