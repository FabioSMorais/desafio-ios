//
//  WebResponse.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import Foundation
import SwiftyJSON

class WebResponse{
    
    var isError : Bool
    var messageError : String
    var json : JSON
    
    init(isError : Bool, messageError : String, json : JSON ){
        self.isError = isError
        self.messageError = messageError
        self.json = json
    }
}
