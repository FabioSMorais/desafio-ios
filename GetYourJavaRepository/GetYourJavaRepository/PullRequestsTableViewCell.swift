//
//  PullRequestsTableViewCell.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import UIKit

class PullRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var uiPullRequestTitle: UILabel!
    @IBOutlet weak var uiUserImage: UIImageView!
    @IBOutlet weak var uiUserName: UILabel!
    @IBOutlet weak var uiDate: UILabel!
    @IBOutlet weak var uiDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.uiPullRequestTitle.text = nil
        self.uiDescription.text = nil
        self.uiUserName.text = nil
        self.uiDate.text = nil
        
        self.uiUserImage.layer.cornerRadius = self.uiUserImage.frame.size.height/2
        self.uiUserImage.layer.masksToBounds = true

        self.uiPullRequestTitle.textColor = UIColor.blue
        self.uiUserName.textColor = UIColor.blue
        self.uiDate.textColor = UIColor.gray
        
    }

    func textForCell(pullRequest : PullRequestResponse){
        
        self.uiPullRequestTitle.text = pullRequest.pullRequestName
        self.uiDescription.text = pullRequest.description
        self.uiUserName.text = pullRequest.userName
        self.uiDate.text = pullRequest.date
        
        let getDate = pullRequest.date
        self.uiDate.text = Utils.dateFormatter(date: getDate)
        
        Utils.getImage(urlString: pullRequest.userImage, imageView: self.uiUserImage)
    }
}
