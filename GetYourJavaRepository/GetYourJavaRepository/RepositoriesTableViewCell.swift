//
//  RepositoriesTableViewCell.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 20/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import UIKit



class RepositoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var uiLblNameRepositories: UILabel!
    @IBOutlet weak var uiLblDescription: UILabel!
    @IBOutlet weak var uiForksNumbers: UILabel!
    @IBOutlet weak var uiFavoriteNumbers: UILabel!
    @IBOutlet weak var uiAuthorImage: UIImageView!
    @IBOutlet weak var uiUserName: UILabel!
    @IBOutlet weak var uiNameLastName: UILabel!
    
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.uiLblNameRepositories.text = nil
        self.uiLblDescription.text = nil
        self.uiForksNumbers.text = nil
        self.uiFavoriteNumbers.text = nil
        self.uiUserName.text = nil
        self.uiNameLastName.text = nil
        
        self.uiAuthorImage.layer.cornerRadius = self.uiAuthorImage.frame.size.height/2
        self.uiAuthorImage.layer.masksToBounds = true
        
        self.uiLblNameRepositories.textColor = UIColor.blue
        self.uiUserName.textColor = UIColor.blue
        self.uiNameLastName.textColor = UIColor.gray
        
        self.uiForksNumbers.textColor = UIColor.yellow
        self.uiFavoriteNumbers.textColor = UIColor.yellow
        
    }
    
    func textForCell(repository : RepositoriesResponse){
        
        self.uiLblNameRepositories.text = repository.repositoryName
        self.uiLblDescription.text = repository.description
        self.uiForksNumbers.text = repository.forksNumbers
        self.uiFavoriteNumbers.text = repository.starsNumbers
        self.uiUserName.text = repository.userName
        self.uiNameLastName.text = repository.fullName
        
        Utils.getImage(urlString: repository.authorImage, imageView: self.uiAuthorImage)

    }
}
