//
//  PullRequestResponse.swift
//  GetYourJavaRepository
//
//  Created by Fabio Souza de Morais on 21/05/17.
//  Copyright © 2017 Fabio Morais. All rights reserved.
//

import Foundation
import SwiftyJSON

class PullRequestResponse{
    
    var pullRequestName : String = ""
    var userName : String = ""
    var date : String = ""
    var userImage : String = ""
    var description : String = ""
    var urlGiHub : String = ""

    class func returnPullRequests(json : JSON) -> [PullRequestResponse]{
        
        var arrayPullRequests = [PullRequestResponse]()
        if let jsonArray = json.array{
            for item in jsonArray{
                let pullRequests = PullRequestResponse()
                
                pullRequests.pullRequestName = item["head"]["repo"]["name"].stringValue
                pullRequests.pullRequestName = item["title"].stringValue
                pullRequests.userName = item["user"]["login"].stringValue
                pullRequests.date = item["updated_at"].stringValue
                pullRequests.userImage = item["user"]["avatar_url"].stringValue
                pullRequests.description = item["body"].stringValue
                pullRequests.urlGiHub = item["user"]["html_url"].stringValue

                arrayPullRequests.append(pullRequests)
            }
            
            return arrayPullRequests
        }
        return[]
    }
}
